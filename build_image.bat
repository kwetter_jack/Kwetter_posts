@echo off
:: variables
set CONTAINER=%1
set DOCKER_USERNAME=jmutsers
set DOCKER_PASSWORD=%2
set GIT_VERSION=%3
set IMAGE=%DOCKER_USERNAME%/%CONTAINER%

echo %IMAGE%

:: Build and set tag for image
docker build -t "%IMAGE%:%GIT_VERSION%" .
docker tag %IMAGE%:%GIT_VERSION% %IMAGE%:latest

:: Log in to Docker Hub and push
docker login -u %DOCKER_USERNAME% -p %DOCKER_PASSWORD%
docker push %IMAGE%:%GIT_VERSION%
docker push %IMAGE%:latest
