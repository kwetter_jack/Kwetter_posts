package com.kwetter.posting_service.helpers.tools;

import com.kwetter.posting_service.objects.models.Post;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class Helper {

    private Helper(){

    }

    public static boolean IsEmpty(String value){
        return value == null || value.isBlank();
    }

    public static <T> List<T> emptyIfNull(List<T> iterable) {
        return iterable == null ? List.of() : iterable;
    }
    public static <T> T[] emptyIfNull(T[] iterable) {
        return iterable == null ? (T[]) List.of().toArray() : iterable;
    }

    public static String getGlobalVariable(String name) throws IOException {
        String val = System.getProperty(name);

        if(IsEmpty(val)){
            Properties configProps = new Properties();
            InputStream iStream = new ClassPathResource("application.properties").getInputStream();
            configProps.load(iStream);

            val = configProps.getProperty(name);
        }

        return val;
    }
}
