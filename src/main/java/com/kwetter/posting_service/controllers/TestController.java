package com.kwetter.posting_service.controllers;

import com.kwetter.posting_service.interfaces.ICommentService;
import com.kwetter.posting_service.interfaces.IPostService;
import com.kwetter.posting_service.interfaces.IUserService;
import com.kwetter.posting_service.objects.data_transfer_objects.PostForAlterationDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/test")
public class TestController {
    private final IPostService postService;
    private final ICommentService commentService;
    private final IUserService userService;

    @Value("${environment}")
    private String environment;

    public TestController(IPostService postService, ICommentService commentService, IUserService userService) {
        this.postService = postService;
        this.commentService = commentService;
        this.userService = userService;
    }

    @PostMapping(path="/addvalues")
    public ResponseEntity<String> addTestValues(HttpServletRequest request, @RequestBody List<PostForAlterationDTO> newPosts){
        String user_id = request.getHeader("x-auth-user-id");

        if(!this.assertIfTestAllowed(user_id)){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        System.out.println("add test values to the post database");

        boolean success = postService.addTestEnvironment(newPosts, user_id);
        return new ResponseEntity<>(success ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

    private boolean assertIfTestAllowed(String user_id){
        if(environment == null || environment.equals("production")){
            return false;
        }

        return user_id.equals("4qZeJ14YeyPGK5pxswgQoQixab53");
    }
}
