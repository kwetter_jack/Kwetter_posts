package com.kwetter.posting_service.controllers;

import static org.junit.jupiter.api.Assertions.*;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.kwetter.posting_service.helpers.HttpService;
import com.kwetter.posting_service.helpers.tools.Helper;
import com.kwetter.posting_service.objects.data_transfer_objects.PostDTO;
import com.kwetter.posting_service.objects.data_transfer_objects.PostForAlterationDTO;
import com.sun.research.ws.wadl.HTTPMethods;
import okhttp3.*;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PostControllerTest {

    private final List<PostForAlterationDTO> newPosts = new ArrayList<>();
    private final HttpService httpService;
    private final MediaType mediaType = MediaType.parse("application/json");
    private final Gson json_converter = new Gson();
    private static boolean setUp = false;

    public PostControllerTest() throws IOException {
        httpService = new HttpService();

        newPosts.add(new PostForAlterationDTO(1, "test message 1"));
        newPosts.add(new PostForAlterationDTO(2, "test message 2"));
        newPosts.add(new PostForAlterationDTO(3, "test message 3"));
        newPosts.add(new PostForAlterationDTO(4, "test message 4"));

        if(!setUp){
            setUp();
        }
    }

    public void setUp() throws IOException {
        String tet = json_converter.toJson(newPosts);
        RequestBody body = RequestBody.create(mediaType, tet);
        String url = "/test/addvalues";

        Response response = httpService.sendHttpMessage(url, body, HTTPMethods.POST);
        setUp = true;
    }

    @Test
    @Order(1)
    void getPost() throws IOException {
        //set up
        int post_id = 2;
        String url = "/post/get/" + post_id;
        String message = newPosts.get(post_id - 1).getMessage();
        PostDTO expectedResult = new PostDTO(2, 0, message, "",httpService.getUserData().getDisplayName(), new ArrayList<>());

        // execution
        Response response = httpService.sendHttpMessage(url, HTTPMethods.GET);

        // assert response
        assertEquals(200, response.code());
        assertNotNull(response.body());

        //get response from body
        ObjectMapper mapper = new ObjectMapper();
        String jsonString = response.body().string();
        PostDTO post = mapper.readValue(jsonString, new TypeReference<PostDTO>(){});

        // assert body
        assertEquals(expectedResult.getId(), post.getId());
        assertEquals(expectedResult.getGroup_Id(), post.getGroup_Id());
        assertEquals(expectedResult.getMessage(), post.getMessage());
        assertEquals(expectedResult.getWriter(), post.getWriter());
        assertEquals(expectedResult.getComments().size(), post.getComments().size());
    }

    @Test
    @Order(2)
    void getPostsFromUser() throws IOException {
        //set up
        String url = "/post/user_posts/" + httpService.getUserData().getLocalId();

        // execution
        Response response = httpService.sendHttpMessage(url, HTTPMethods.GET);

        // assert response
        assertEquals(200, response.code());
        assertNotNull(response.body());

        //get response from body
        ObjectMapper mapper = new ObjectMapper();
        String jsonString = response.body().string();
        List<PostDTO> posts = mapper.readValue(jsonString, new TypeReference<List<PostDTO>>(){});

        // assert body
        assertEquals(4, posts.size());

        int count = 0;
        for(PostDTO post : posts){
            if(count > 0){
                PostDTO previousPost = posts.get(count-1);
                LocalDateTime t = post.getCreation_date();
                LocalDateTime r = previousPost.getCreation_date();
                assertTrue(post.getCreation_date().isBefore(previousPost.getCreation_date()));
            }
            assertFalse(Helper.IsEmpty(post.getMessage()));

            if(posts.size() - count <= 4){
                int index = posts.size() - (count+1);
                assertEquals(post.getMessage(), newPosts.get(index).getMessage());
            }

            count++;
        }
    }

    @Test
    @Order(3)
    void updatePost() throws IOException {
        //set up
        String message = "dit is mijn nieuwe message voor post 2";
        RequestBody body = RequestBody.create(mediaType, "{\"id\": 2,\"message\": \""+message+"\"}");
        String url = "/post/update";

        // execute
        Response response = httpService.sendHttpMessage(url, body, HTTPMethods.PUT);

        //assert
        assertEquals(200, response.code());
        assertNotNull(response.body());

        PostDTO result = json_converter.fromJson(response.body().string(), PostDTO.class);

        assertEquals(2, result.getId());
        assertEquals(message, result.getMessage());
        assertEquals(0, result.getGroup_Id());
        assertEquals(httpService.getDisplayName(), result.getWriter());
    }

    @Test
    @Order(4)
    void deletePost() throws IOException {
        //set up
        int postId = 3;
        String url = "/post/delete/"+ postId;
        RequestBody body = RequestBody.create(mediaType, "");

        // execute
        Response response = httpService.sendHttpMessage(url, body, HTTPMethods.DELETE);

        //assert
        assertEquals(200, response.code());

        // get all posts
        url = "/post/user_posts/" + httpService.getUserData().getLocalId();

        // execution
        response = httpService.sendHttpMessage(url, HTTPMethods.GET);

        // assert response
        assertEquals(200, response.code());
        assertNotNull(response.body());

        //get response from body
        ObjectMapper mapper = new ObjectMapper();
        String jsonString = response.body().string();
        List<PostDTO> posts = mapper.readValue(jsonString, new TypeReference<List<PostDTO>>(){});

        // assert body
        assertEquals(3, posts.size());

        PostDTO post = posts.stream().filter(postDTO -> postDTO.getId() == postId).findFirst().orElse(null);
        assertNull(post);
    }

    @Test
    @Order(5)
    void createPost() throws IOException {
        int groupId = 0;
        String message = "dit is mijn post message";

        RequestBody body = RequestBody.create(mediaType, "{\"group_Id\": " + groupId + ",\"message\": \""+ message +"\"}");
        String url = "/post/create";

        Response response = httpService.sendHttpMessage(url, body, HTTPMethods.POST);

        assertEquals(201, response.code());
        assertNotNull(response.body());

        PostDTO result = json_converter.fromJson(response.body().string(), PostDTO.class);

        assertEquals(message, result.getMessage());
        assertEquals(groupId, result.getGroup_Id());
        assertEquals(httpService.getDisplayName(), result.getWriter());
    }

}
